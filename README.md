# android_vendor_xiaomi_alioth-firmware

Firmware images for POCO F3 (alioth), to include in custom ROM builds.

**Current version**: fw_alioth_miui_ALIOTHGlobal_V13.0.7.0.SKHMIXM_73b8f668f8_12.0.zip

### How to use?

1. Clone this repo to `vendor/xiaomi/alioth-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/alioth-firmware/BoardConfigVendor.mk
```
